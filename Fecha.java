package Activitat13;

import java.util.StringTokenizer;

public class Fecha {

    private int dia;

    private int mes;

    private int anyo;

    private static final String[] DIAS_TEXTO = new String[] { "domingo", "lunes", "martes", "miercoles", "jueves", "viernes",
            "sábado"};

    private static final String[] MESES_TEXTO = new String[] { "enero", "febrero", "marzo", "abril", "mayo", "junio",
            "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };

    /**
     *  Constructor por defecto
     *  Inicializa una fecha a dia 1-1-1970
     */
    public Fecha() {
        this.dia = 1;
        this.mes = 1;
        this.anyo = 1970;
    }

    /**
     *  Inicializa la fecha
     *  @param dia de la semana
     *  @param mes del año
     *  @param anyo
     */
    public Fecha(int dia, int mes, int anyo) {
        this.dia = dia;
        this.mes = mes;
        this.anyo = anyo;
    }

    /**
     * Inicializa la fecha a partir de otra pasada en formato String dd/mm/yyyy
     *
     * Deberemos trocearlas de forma que asignemos el día més y año a cada uno de los atributoe
     * @param fecha
     */
    public Fecha(String fecha) {
        StringTokenizer st = new StringTokenizer(fecha);
        this.dia = Integer.valueOf(st.nextToken());
        this.mes = Integer.valueOf(st.nextToken());
        this.anyo = Integer.valueOf(st.nextToken());
        }


    /**
     * Modifica la fecha actual a partir de los datos pasados como argumento
     */
    public void set(int dia, int mes, int anyo) {
        this.dia = dia;
        this.mes = mes;
        this.anyo = anyo;
    }
    public Fecha clone(Fecha fecha)
    {
        Fecha fechaClon=fecha;
        return fechaClon;
    }

    /**
     * Devuelve el día de la semana que representa por la Fecha actual
     * @return @dia
     */
    public int getDia() {
        return dia;
    }

    /**
     * Devuelve el mes que representa la Fecha actual
     * @return @mes
     */
    public int getMes(){
        return mes;
    }

    /**
     * Devuelve el año que representa la Fecha actual
     * @return @mes
     */
    public int getAnyo(){
        return anyo;
    }

    /**
     * Muestra por pantalla la fecha en formato español dd-mm-yyyy
     */
    public void mostrarFormatoES()  {
        System.out.println(""+this.dia+" - "+this.mes+" - "+this.anyo);
    }

    /**
     * Muestra por pantalla la fecha en formato inglés yyyy-mm-dd
     */
    public void mostrarFormatoGB() {
        System.out.println(""+this.anyo+" - "+this.mes+" - "+this.dia);
    }

    /**
     * Muestra por pantalla la fecha en formato texto dd-mmmm-yyyy
     * Ej. 1 enero de 1970
     */
    public void mostrarFormatoTexto() {

                System.out.println(dia+" "+getMesTexto()+" de "+anyo);
            }


    /**
     * Retorna un booleano indicando si la fecha del objeto es igual a la fecha pasada como
     * argumento
     *
     * @return boolean
     */
    public boolean isEqual(Fecha otraFecha) {
        if (dia==otraFecha.dia&&mes==otraFecha.mes&&anyo==otraFecha.anyo) {
            return true;
        }
        return false;
    }

    /**
     * Devuelve el número de la semana dentro del año actual.
     *
     * @return int dia semana
     */
    public int getNumeroSemana() {

        return 0;
    }

    /**
     * Retorna el dia correspondiente de la semana en formato String
     * @return String
     */
    public String getDiaSemana() {
        int resto = getDiasTranscurridosOrigen() % 7;
        return DIAS_TEXTO[resto];
    }

    /**
     * Solo Festivo sábado o domingo
     * @return boolean
     */
    public boolean isFestivo() {
        if (getDiasTranscurridosOrigen() % 7 == 0||getDiasTranscurridosOrigen()%7 ==6) {
            return true;
        }
        return false;
    }

    /**
     * Devuelve un objeto de tipo fecha que representa una fecha añadiendo @numDias
     * A la fecha Actual. El número máximo de dias a restar es 30
     *
     * @return boolean
     */
    public Fecha anyadir(int numDias) {
       set((dia+numDias),mes,anyo);
       if (dia>30){
           set((dia-30),mes+1,anyo);
       }
       return null;
    }

    /**
     * Devuelve un objeto de tipo fecha que representa una fecha restando @numDias
     * A la fecha Actual. El número máximo de dias a restar es 30
     *
     * @return boolean
     */
    public Fecha restar(int numDias){
        set((dia-numDias),mes,anyo);
        if (dia>30){
            set((dia+30),mes+1,anyo);
        }
        return null;
    }

    public boolean isCorrecta(){
        if (isBisiesto(dia)) {
            return true;
        }
        return false;
    }

    /**
     * Retorna el mes del año en formato text (enero, febrero, marzo,...)
     * @return char
     */
    private String getMesTexto() {
        String mesTexto = " ";
        for (int i = 0; i < MESES_TEXTO.length; i++) {
            if (this.mes == i + 1) {
                System.out.println(MESES_TEXTO[i]);
                mesTexto = MESES_TEXTO[i];

            }

        }
        return mesTexto;
    }

    /**
     * Devuelve el número de dias transcurridos desde el 1-1-1
     *
     * @return int
     */
    private int getDiasTranscurridosOrigen() {
        int total = 0;
        total = total + getDiasTranscurridosAnyo();

        for (int i = 1; i < anyo; i++) {
            total = total + getDiasAnyo(i);
        }

        return total;
    }

    /**
     * Devuelve el número de dias transcurridos en el anyo que representa el objeto
     *
     * @return int
     */
    private int getDiasTranscurridosAnyo() {
        int total = 0;

        for (int i = 1; i < mes; i++) {
            total=total+getDiasMes((i),anyo);
        }

        return total + dia;

    }

    /**
     * Indica si el año pasado como argumento es bisiesto. Un año es bisiesto si es divisible por 4
     * pero no es divisible entre 100 o es divisible entre 4 entre 100 y entre 400
     *
     * @return boolean
     */
    public static boolean isBisiesto(int anyo){
        if ((anyo%4==0&&anyo%100!=0)||(anyo%4==0&&anyo%100==0&&anyo%400==0)){
            return true;
        }else {
            return false;
        }
    }

    /**
     *  Calcula el número de días que tiene el @mes en el @año pasado como argumento
     *  Deberás hacer uso del métodos isBisiesto
     *
     *  @return int total dias mes en curso
     */
    public static int getDiasMes(int mes, int anyo) {
        switch (mes) {
            case 1:
                return 31;
            case 2:
                if (isBisiesto(anyo) == true) {
                    return 29;
                } else {
                    return 28;
                }
            case 3:
                return 31;
            case 4:
                return 30;
            case 5:
                return 31;
            case 6:
                return 30;
            case 7:
                return 31;
            case 8:
                return 31;
            case 9:
                return 30;
            case 10:
                return 31;
            case 11:
                return 30;
            case 12:
                return 31;
            default:
                System.out.println("El mes no es válido");
        }
        return 0;
    }

    /**
     * Calcula el número total de dias que tiene el año pasado como argumento
     *
     * @return int total dias anyo en curso
     */
    public static int getDiasAnyo(int anyo){
        if(isBisiesto(anyo)) {
            return 366;
        }
        return 365;
    }
}


