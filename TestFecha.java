package Activitat13;

public class TestFecha {
    /**
     * A Continuación se presentan algunos ejemplos de uso de la clase Fecha, que deberas borrar
     * Para realizar el ejercicio.
     *
     * @param args
     */
    public static void main(String[] args) {

            Fecha fechaEpoch = new Fecha();

            System.out.println("El tiempo epoch es");
            fechaEpoch.mostrarFormatoTexto();

            Fecha fechaActual = new Fecha(16, 1, 2021);

            System.out.println("La fecha actual es festiva --> " + fechaActual.isFestivo());
            System.out.println("El año actual es bisiesto  --> " + Fecha.isBisiesto(fechaActual.getAnyo()));

            System.out.println("La fecha actual para un usuario español es");
            fechaActual.mostrarFormatoES();
        System.out.println("Dia semana");
        System.out.println(fechaActual.getDiaSemana());
        System.out.println("Es correcta");
        System.out.println(fechaActual.isCorrecta());

            System.out.println("La fecha actual para un usuario ingles es");
            fechaActual.mostrarFormatoGB();

            System.out.println("El dia es " + fechaActual.getDia());
            fechaActual.anyadir(4);
            System.out.println("El dia tras añadir 4 dias es " + fechaActual.getDia());
        }
    }




